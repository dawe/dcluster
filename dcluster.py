#import sklearn.cluster
import numpy as np
import sys
import argparse
import scipy.stats
import scipy.integrate
from scipy.spatial.distance import pdist, cdist, squareform
from scipy.signal import savgol_filter
import os
from sklearn.neighbors import NearestNeighbors
from collections import Counter
import networkx as nx
import community



def subsample_df(data, sub_ratio):
	idx = np.arange(len(data))
	np.random.shuffle(idx)
	l = int(len(idx) * sub_ratio)
	return data[idx[:l]]


def estimate_cutoff(distance_matrix):
	# this function is here for the future... it's broken right now

	# here we estimate DBSCAN parameters. Distance cutoff is intrinsecally linked
	# to minimal number of features in a cluster. I've tried to apply rules 
	# specified in Ester M. et al (DBSCAN original paper) but there is no
	# "valley" in k-dist graphs (at least in my hands). I've tried another strategy

	# first of all get all nearest neighbors, here we stay large, 100 nearest neighbors
	# are typically a lot

	n_nearest = min(100, len(distance_matrix))
	nn = NearestNeighbors(n_nearest, metric='precomputed', p=1)
	nn.fit(distance_matrix)
		
	nn_dist, nn_id = nn.kneighbors()

	# now look for the max value of the n-nearest neighbors, and look for the one which is
	# just lower than the average distance (that is, equally distributed)
	# log scale...
	
	k_hist = [np.histogram(np.log(nn_dist[:, x] ) , bins=20 ) for x in range(n_nearest)]
	
	eq_d = np.log(np.max(distance_matrix) / len(distance_matrix) )
	
	k_max = np.array([k_hist[x][1][np.argmax(k_hist[x][0])] for x in range(n_nearest)] )

	d1 = np.gradient(k_max)
	svf = savgol_filter(d1, 5, 2)[k_max < eq_d]
	
	n_samples = np.argmin(svf) + 1
	n_samples = max(3, n_samples)
	
	k_max = k_max[len(svf)]

	est_eps = int(np.exp(np.max(k_max)))
		
	return (est_eps, n_samples)

def calculate_distance(data, method='exponential', k=1000., anchor=0):
	if anchor:
		distance_matrix = end_to_end_dist(data)
	else:
		mid_points = (data[:, 0] + data[:, 1]) // 2
		distance_matrix = squareform(np.array(pdist(mid_points[:, None], metric = 'cityblock')))
	if k <= 0:
		k = 1
	
	if method == 'exponential':
		distance_matrix = 1 - np.exp(-distance_matrix / k)

	return distance_matrix


	
def end_to_end_dist(data):
	#assume sorted
	#take only upper triangular matrix, so that distances are, in general, positive
	#negative distances will be treated as overlapping intervals, no abs at this point
	dm = np.array(cdist(data[:, 0][:, None], data[:, 1][:, None], metric = 'cityblock'), dtype=np.int32)
	dm = np.triu(dm - np.diag(dm)[:, None])
	dm[dm < 0] = 0 #in case there are overlapping intervals, distances should be set to 0
	return dm + dm.T #copy upper triangle in lower and return

def dbscan_cluster(distance_matrix, eps, min_features = 3):
	from sklearn.cluster import DBSCAN
	db = DBSCAN(eps = eps, min_samples = min_features, metric = 'precomputed', n_jobs = os.cpu_count())
	db.fit(distance_matrix)
	return db.labels_

def graph_cluster(M, min_features = 3, resolution = 1.):
	if len(M) <= min_features:
		return -1 * np.ones(len(M))

	D = 1 - M #this is necessary as weights are from 0 to 1, while distances are 1 to 0
	D = D - np.eye(len(D)) # remove self-loops	

	G = nx.from_numpy_array(D)
	P = community.best_partition(G, resolution = resolution)
	
	clusters = np.array([P[x] for x in range(len(G.nodes()))])
	U, C = np.unique(clusters, return_counts = True)
	
	# if some partitions are less than min_samples, mark them as unclustered
	for x in range(len(U)):
		if C[x] < min_features:
			clusters[clusters == U[x]] = -1
			
	return clusters			

def cluster_intervals():
	option_parser = argparse.ArgumentParser(
	description = "Apply DBSCAN clustering procedure to a set of intervals",
	prog = "dcluster.py", 
	)
	option_parser.add_argument("--input", "-i", help = "Input file in BED format", action = "store", type = argparse.FileType('r'), default = sys.stdin)
	option_parser.add_argument("--output", "-o", help = "Output file", action = "store", type = argparse.FileType('w'), default = sys.stdout)
	option_parser.add_argument("--distance-cutoff", "-d", help = "Distance cutoff between two features", action = "store", type = float, default = 5000)
	option_parser.add_argument("--min-features", "-m", help = "Minimum number of features to be considered a cluster", action = "store", type = int, default = 3)
	option_parser.add_argument("--output-unclustered", "-u", help = "Ouput unclustered featurles", action = "store_true", default = False)
	option_parser.add_argument("--distance-anchor", "-D", help="Evaluate distances from interval mid points (0) or interval ends (1) [default: 0]", type = int, default = 0)
	option_parser.add_argument("--distance-method", "-M", help="Evaluate distances on linear or exponential scale [default: exponential]", type = str, choices=['exponential', 'linear'], default = 'exponential')
	option_parser.add_argument("--distance-decay", "-k", help="Rate of decay for distance evaluation in bp (default 1000)", type = float, default = 1000)
#	option_parser.add_argument("--statistics", "-s", help = "Perform statistical analysis on final intervals (e.g. when starting from raw reads)", action = "store_true", default = False)
#	option_parser.add_argument("--weighted", "-w", help = "Weight data using information in this column", type = int)
	option_parser.add_argument("--cluster-method", "-C", help="Cluster intervals using DBSCAN or Graph-based approach (default: DBSCAN)", type = str, choices=['DBSCAN', 'graph'], default = 'DBSCAN')
	option_parser.add_argument("--knn", help="K-nearest neighbors for Graph based clustering", type=int, default=5)
	option_parser.add_argument("--resolution", "-R", help="Resolution for Louvain clustering", type=float, default=1.0)
	option_parser.add_argument("--subsample", "-S", help= "Random pick such fraction of data if memory is an issue (0 = disable)", action = "store", type = float, default = 0.0)
	option_parser.add_argument("--version", "-v", action="version", version = "%(prog)s 0.5")

	options = option_parser.parse_args()
	
	file_in = options.input
	chrom_array = []
	start_array = []
	end_array = []
	
	
	for nl, line in enumerate(file_in):
		if line.startswith('#') or line.startswith(' ') or line.startswith('!') or line.startswith('\t'):
			continue
		fields = line.split() #here space or tab are allowed
		chrom_array.append(fields[0])
		try:
			start_array.append(int(fields[1]))
		except ValueError:
			sys.exit("Not an integer in start column, line %d\n" % nl)
		try:
			end_array.append(int(fields[2]))
		except ValueError:
			sys.exit("Not an integer in end column, line %d\n" % nl)
		
	chrom_array = np.array(chrom_array)
	data = np.array(np.vstack([np.array(start_array), np.array(end_array)]).T, dtype=np.int32) #int32 because we don't need 64bit integers... do we?
	
	chromosome_counter = Counter(chrom_array)
	chromosomes= [x[0] for x in chromosome_counter.most_common()]
	counts = [x[1] for x in chromosome_counter.most_common()]

	# ordered from decreasing number of elements, so that the first chromosome analyzed
	# will hopefully have the highest number of elements to estimate distance cutoff
	
	sub_ratio = np.abs(options.subsample)
	sub_ratio = sub_ratio - int(np.floor(sub_ratio))

	distance_cutoff = options.distance_cutoff	
	min_features = max(options.min_features, 3)
	
	if distance_cutoff < 0:
		distance_cutoff = np.abs(distance_cutoff)


	if options.distance_method == 'exponential':
		distance_cutoff = 1 - np.exp(-distance_cutoff / options.distance_decay)

	for n_chrom, chrom in enumerate(chromosomes):
		chrom_mask = chrom_array == chrom
		#analyze a single chromosome
		sub_data = data[chrom_mask]

		if sub_ratio > 0 and sub_ratio < 1:
			sub_data = subsample_df(sub_data, sub_ratio)

		if len(sub_data) == 0:
			sys.stderr.write("No data for chromosome %s\n" % chrom)
			continue
		if len(sub_data) > 15000:
			# I've found pdist/cdist have memory issues with large dataset.
			sys.stderr.write("Warning, high number of features detected, consider subsampling\n")

		#order data by start position
		
		sub_data = sub_data[np.argsort(sub_data[:, 0])]	
		
		distance_matrix = calculate_distance(sub_data, method=options.distance_method, k=options.distance_decay, anchor=options.distance_anchor)


		if len(sub_data) < min_features:
			cluster_labels = -1 * np.ones(len(sub_data))
		else:
			sys.stderr.write("Fitting data on chromosome %s (%d)\n" % (chrom, len(sub_data)))
			if options.cluster_method == 'DBSCAN':
				cluster_labels = dbscan_cluster(distance_matrix, distance_cutoff, min_features)	
			elif options.cluster_method == 'graph':
				cluster_labels = graph_cluster(distance_matrix, min_features, options.resolution)	
			



		sub_data = np.hstack([sub_data, cluster_labels[:, None]]) #add labels to data
		for label in np.unique(cluster_labels):
			clustered_mask = sub_data[:, 2] == label
			clustered_features = sub_data[clustered_mask]
			cluster_name = "%d_%d" % (label, n_chrom)
			if label == -1:
				if options.output_unclustered:
					for feature in range(len(clustered_features)):
						fname = "u_%d_%d" % (feature, n_chrom)
						start = clustered_features[feature, 0]
						end = clustered_features[feature, 1]
						options.output.write("%s\t%d\t%d\t%s\t1\n" % (chrom, start, end, fname))
				else:
					continue
			else:
				n_merged = len(clustered_features)
				start = np.min(clustered_features[:, 0])
				end = np.max(clustered_features[:, 1])
				options.output.write("%s\t%d\t%d\t%s\t%d\n" % (chrom, start, end, cluster_name, n_merged))

if __name__ == '__main__':
	cluster_intervals()
